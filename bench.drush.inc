<?php
define('DRUSH_RED', "\033[0;31m\033[1m%s\033[0m");
define('DRUSH_GREEN', "\033[0;32m\033[1m%s\033[0m");

/**
 * Implementation of hook_drush_command().
 */
function bench_drush_command() {
  $items['bench'] = array(
    'description' => '.',
    'arguments' => array(
      'calback' => 'Menu callback.',
    ),
    'options' => array(
      'reps' => 'Number of requests',
      'patch' => 'Patch to apply',
      'features-revert' => 'Perform a features-revert after applying patch.',
    ),
  );
  $items['bench-callback'] = array(
    'arguments' => array(
      'callback' => 'Menu callback',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
    'hidden' => TRUE,
  );

  return $items;
}

function drush_bench($callback) {
  $runs = array();
  $reps = drush_get_option('reps', 1);
  $patch = drush_get_option('patch', FALSE);

  $db = _drush_sql_get_db_spec();
  // Dump/restore original db
  if ($patch) {
    drush_shell_exec('mysqldump -u%s -p%s -h%s %s | gzip > /tmp/benchsqldump.sql.gz', $db['username'], $db['password'], $db['host'], $db['database']);
    drush_shell_exec('gunzip -c /tmp/benchsqldump.sql.gz | mysql -u%s -p%s -h%s %s', $db['username'], $db['password'], $db['host'], $db['database']);
  }

  $runs[] = _drush_bench($callback, $reps);
  if ($patch) {
    $patch_path = dirname($patch);
    $patchfile = basename($patch);
    foreach (array(0,1) as $l) {
      if (drush_shell_cd_and_exec($patch_path, 'patch -f --dry-run -p' . $l . ' < %s', $patchfile)) {
        drush_shell_cd_and_exec($patch_path, 'patch  -p' . $l . ' < %s', $patchfile);
        break;
      }
    }

    // Restore db
    drush_shell_exec('gunzip -c /tmp/benchsqldump.sql.gz | mysql -u%s -p%s -h%s %s', $db['username'], $db['password'], $db['host'], $db['database']);
    $runs[] = _drush_bench($callback, $reps, TRUE);
    drush_shell_cd_and_exec($patch_path, 'patch -R -p' . $l . ' < %s', $patchfile);
  }
  if (count($runs) === 2) {
    $run_ids = array($runs[0]['run-id'], $runs[1]['run-id']);
  }
  else {
    $run_ids = $runs[0]['run-id'];
  }
  $namespace = $runs[0]['namespace'];

  drush_print_table(drush_xhprof_results_table($run_ids, $namespace), TRUE);

  // Only print the xhprof link if we know where the xhprof source is.
  if (drush_get_option('drush-xhprof-directory', FALSE)) {
    $link = drush_xhprof_link($run_ids, $namespace);
    drush_print("XHProf report: ");
    drush_print($link);
  }
}

function _drush_bench($callback, $reps, $patch = FALSE) {
  $runs = array();

  if (drush_get_option('features-revert', FALSE)) {
    drush_invoke_process('@self', 'fra', array(), array('-y'));
  }
  $mess = $patch ? "Starting run - PATCH" : "Starting run";
  drush_log($mess);

  // Call once to prime caches before recording results so we don't throw off averages.
  drush_invoke_process('@self', 'bench-callback', array($callback));

  for ($i = 0; $i < $reps; $i++) {
    drush_log("Run rep " . $i);
    $backend_opts = array('integrate' => TRUE);
    $res = drush_invoke_process('@self', 'bench-callback', array($callback), array(), $backend_opts);

    if ($res['error_status'] === 0) {
      if (!empty($res['object'])) {
        $runs[] = $res['object'];
      }
      else {
        drush_set_error('DRUSH_BENCH_ERROR', dt("Failed to get XHProd run-id."));
      }
    }
    else {
      if (!empty($res['error_log'])) {
        foreach ($res['error_log'] as $key => $err) {
          drush_set_error($key, $err);
          return;
        }
      }
    }
  }

  drush_xhprof_include();
  $run_data = array();
  if (!empty($runs)) {
    foreach ($runs as $run) {
      $desc = '';
      $run_data[] = drush_bench_get_run($run['run-id'], $run['namespace'], $desc);
    }
    $agg = XHProfTools::combine($run_data);
    $run_id = drush_bench_save_run($agg, $run['namespace']);
    $namespace = $run['namespace'];
    return array('run-id' => $run_id, 'namespace' => $namespace);
  }
  else {
    drush_set_error('DRUSH_BENCH_ERROR', dt("No XHProf runs to aggregate!"));
  }
}

function drush_bench_validate($callback = NULL) {
  if (!$callback) {
    drush_set_error('DRUSH_BENCH_ERROR', dt("Must specify a valid path!"));
  }
}

function drush_xhprof_link($run_id, $namespace) {
  $xhprof_url = drush_get_option('drush-xhprof-url', '');
  if ($xhprof_url) {
    if (is_array($run_id) && count($run_id) === 2) {
      $url  = $xhprof_url . '/index.php?run1=' . $run_id[0] . '&run2=' . $run_id[1] . '&source=' . $namespace;
    }
    else {
      $url  = $xhprof_url . "/index.php?run=$run_id&source=$namespace";
    }
    return $url;
  }
}


function drush_bench_callback($callback) {
  // Set up some varaibles to avoid notices.
  $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'] = $_SERVER['PHP_SELF'];
  $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
  $_SERVER['REQUEST_METHOD']  = 'GET';
  $_SERVER['SERVER_SOFTWARE'] = 'Apache/2.2.19';
  $_SERVER['HTTP_USER_AGENT'] = NULL;

  // Set the page callback.
  $_GET['q'] = $callback;
  drush_xhprof_enable();

  $drupal_version = drush_drupal_major_version();
  $bench_callback = 'drush_bench_callback_' . $drupal_version;
  if (function_exists($bench_callback)) {
    $bench_callback($callback);
  }
  else {
    drush_set_error('DRUSH_BENCH_ERROR', dt("No callback for Drupal version !version!", array('!version' => $drupal_version)));
  }

  $run_info = drush_xhprof_disable();
  drush_backend_set_result($run_info);
}

/**
 * Modified version of D6's index.php.
 */
function drush_bench_callback_6($callback) {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

  global $user;
  $uid = drush_get_option(array('u', 'user'), 0);
  $drush_user = user_load(array('uid' => $uid));
  $user = $drush_user;
  session_save_session(FALSE);

  $return = menu_execute_active_handler();

  // Menu status constants are integers; page content is a string.
  if (is_int($return)) {
    switch ($return) {
    case MENU_NOT_FOUND:
      drupal_not_found();
      break;
    case MENU_ACCESS_DENIED:
      drupal_access_denied();
      break;
    case MENU_SITE_OFFLINE:
      drupal_site_offline();
      break;
    }
  }
  elseif (isset($return)) {
    // Print any value (including an empty string) except NULL or undefined:
    $page = theme('page', $return);
    if (drush_get_context('DRUSH_DEBUG') && !drush_get_context('DRUSH_QUIET')) {
      drush_print($page);
    }
  }

  drupal_page_footer();
}

/**
 * Modified version of D7's index.php.
 */
function drush_bench_callback_7($callback) {
  // The DRUPAL_ROOT constant will already be defined in drush5.
  if (!defined('DRUPAL_ROOT')) define('DRUPAL_ROOT', getcwd());

  require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

  global $user;
  $uid = drush_get_option(array('u', 'user'), 0);
  $drush_user = user_load($uid);
  $user = $drush_user;
  drupal_save_session(FALSE);
  // Put in a blank callback for ob_start since this gets flushed in common.inc.
  $page = '';
  ob_start(function($buffer) use(&$page) {
    $page = $buffer;
  });
  menu_execute_active_handler();
  $page_output = $page;
  ob_end_clean();
  if (drush_get_context('DRUSH_DEBUG') && !drush_get_context('DRUSH_QUIET')) {
    drush_print($page_output);
  }
}

/**
 * Just pass to the D7 callback until this is different.
 */
function drush_bench_callback_8($callback) {
  drush_bench_callback_7($callback);
}

function drush_xhprof_include() {
  static $included = FALSE;
  if (!$included) {
    if (extension_loaded('xhprof')) {
      if ($path = drush_get_option('drush-xhprof-directory', FALSE)) {
        include_once $path . '/xhprof_lib/utils/xhprof_lib.php';
        include_once $path . '/xhprof_lib/utils/xhprof_runs.php';
      }
      $bench_path = dirname(__FILE__);
      require($bench_path . '/XHProfParser.php');
      require($bench_path . '/XHProfDiffParser.php');
      $included = TRUE;
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}

function drush_xhprof_enable() {
  if (drush_xhprof_include()) {
    xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
  }
}

function drush_xhprof_disable() {
  $namespace = 'drush-bench-' . variable_get('site_name', '');
  $xhprof_data = xhprof_disable();
  $runid = drush_bench_save_run($xhprof_data, $namespace);
  return array('run-id' => $runid, 'namespace' => $namespace);
}

function drush_xhprof_results_table($run_ids, $namespace) {
  $desc = '';
  $descriptions = array(
    'ct' => 'Number of function Calls',
    'wt' => 'Incl. Wall Time (microsec)',
    'cpu' => 'Incl. CPU (microsecs)',
    'mu' => 'Incl. MemUse (bytes)',
    'pmu' => 'Incl. PeakMemUse (bytes)',
  );

  $rows = array();

  if (is_array($run_ids)) {
    $xhprof_data1 = drush_bench_get_run($run_ids[0], $namespace, $desc);
    $xhprof_data2 = drush_bench_get_run($run_ids[1], $namespace, $desc);
    $parser = new XHProfDiffParser($xhprof_data1, $xhprof_data2);
    $totals = $parser->getDiffTotals();
    $rows = array();
    $header = array("");
    foreach ($totals as $key => $data) {
      if (is_numeric($key)) {
        $run_key = $key + 1;
        $header[] = "Run" . $run_key . " #" . $run_ids[$key];
      }
      else {
        $header[] = ucfirst($key);
      }
    }
    $rows[] = $header;
    foreach ($descriptions as $metric => $metric_desc) {
      $row = array($metric_desc);
      foreach ($totals as $key => $metrics) {
        // Only use coloring on diffs
        if (is_numeric($key)) {
          $row[] = number_format($metrics[$metric]);
        }
        else {
          $row[] = drush_bench_number_format($metrics[$metric]);
        }
      }
      $rows[] = $row;
    }
  }
  else {
    $xhprof_data = drush_bench_get_run($run_ids, $namespace, $desc);
    $parser = new XHProfParser($xhprof_data);
    $totals = $parser->getTotals();

    $rows[] = array("", "Run #" . $run_ids);
    foreach ($totals as $metric => $value) {
      $rows[] = array($descriptions[$metric], number_format($value));
    }
  }

  return $rows;
}

function drush_bench_number_format($num) {
  $num = is_float($num) ? number_format($num, 2) : number_format($num);
  if ($num != 0) {
    $color = $num > 0 ? DRUSH_RED : DRUSH_GREEN;
    return sprintf($color, $num);
  }
  else {
    return $num;
  }
}

/**
 * Mostly copied from XHProfRuns_Default()->save_run.
 *
 * @param $xhprof_data
 * @param $type
 * @param $run_id
 *
 * @return
 */
function drush_bench_save_run($xhprof_data, $type, $run_id = null) {
  // Use PHP serialize function to store the XHProf's
  // raw profiler data.
  $xhprof_data = serialize($xhprof_data);

  if ($run_id === null) {
    $run_id = uniqid();
  }

  $file_name = drush_bench_file_name($run_id, $type);

  $file = fopen($file_name, 'w');
  if ($file) {
    fwrite($file, $xhprof_data);
    fclose($file);
  }
  else {
    watchdog("xhprof", "Could not open %filename.", array('%file_name' => $file_name));
  }

  return $run_id;
}


/**
  * Mostly copied from XHProfRuns_Default()->get_run.
  *
  * @param $run_id
  * @param $type
  * @param $run_desc
  *
  * @return
 */
function drush_bench_get_run($run_id, $type, &$run_desc) {
  $file_name = drush_bench_file_name($run_id, $type);

  if (!file_exists($file_name)) {
    drush_set_error('DRUSH_BENCH_ERROR', dt("Could not find file $file_name"));
    $run_desc = "Invalid Run Id = $run_id";
    return null;
  }

  $contents = file_get_contents($file_name);
  $run_desc = "XHProf Run (Namespace=$type)";
  return unserialize($contents);
}


function drush_bench_file_name($run_id, $type) {
  $file = "$run_id.$type.xhprof";

  $dir = ini_get("xhprof.output_dir") ?: '/tmp';
  if (!empty($dir)) {
    $file = $dir . "/" . $file;
  }
  return $file;
}

class XHProfTools {
  public static function combine($run_data) {
    $keys = array();
    foreach ($run_data as $data) {
      $run_data[] = $data;
      $keys = $keys + array_keys($data);
    }
    $agg_run = array();
    $run_count = count($run_data);
    foreach ($keys as $key) {
      $agg_key = array();
      // Check which runs have this parent_child function key, collect metrics if so.
      foreach ($run_data as $data) {
        if (isset($data[$key])) {
          foreach ($data[$key] as $metric => $val) {
            $agg_key[$metric][] = $val;
          }
        }
      }

      // Average each metric for the key into the aggregated run.
      $agg_run[$key] = array();
      foreach ($agg_key as $metric => $vals) {
        $sd = self::sd($vals);
        $mean = (array_sum($vals) / count($vals));
        $good_vals = array();

        if ($sd == 0) {
          $agg_run[$key][$metric] = (array_sum($vals) / count($vals));
        }
        else {
          foreach ($vals as $v) {
            $diff = abs($mean - $v);
            if (abs($mean - $v) < ($sd * 2)) {
              $good_vals[] = $v;
            }
          }
          $agg_run[$key][$metric] = (array_sum($good_vals) / count($good_vals));
        }
      }
    }

    return $agg_run;
  }

  public static function sd_square($x, $mean) {
    return pow($x - $mean,2);
  }

  /**
   * Function to calculate standard deviation (uses sd_square)
   */
  public static function sd($array) {
    // square root of sum of squares devided by N-1
    return sqrt(array_sum(array_map(array('XHProfTools', 'sd_square'), $array, array_fill(0, count($array), (array_sum($array) / count($array))))) / (count($array)-1));
  }
}
